export default class CacheManager {
  constructor () {
    this.CACHE = 'cache-update-and-refresh'
  }

  register () {
    if (typeof navigator !== 'undefined' && 'serviceWorker' in navigator) {
      navigator.serviceWorker.register('/service-worker.js', {scope: '/'})
    }
  }

  onOutdated (callback) {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.onmessage = (evt) => {
        var message = JSON.parse(evt.data)

        var isRefresh = message.type === 'refresh'
        var isAsset = message.url.includes('asset')
        var lastETag = localStorage.currentETag
        var isNew = lastETag !== message.eTag

        if (isRefresh && isAsset && isNew) {
          if (lastETag) {
            callback()
          }

          localStorage.currentETag = message.eTag
        }
      }
    }
  }

  onReady (callback) {
    if (typeof navigator !== 'undefined' && 'serviceWorker' in navigator) {
      navigator.serviceWorker.ready.then(callback)
    }
  }
}
