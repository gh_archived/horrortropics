// Import Node Modules
import RouteRecognizer from 'route-recognizer'

// Import Route Data
import routes from '../Data/Routes.jsx'
import { hostname, port } from '../Data/Settings.js'

// Require isomorphic-fetch polyfill
require('isomorphic-fetch')

export default class RouteManager {
  constructor () {
    this.routeRecognizer = new RouteRecognizer()
    this.setupRoutes()
  }

  setupRoutes () {
    for (var i = 0; i < routes.length; i++) {
      this.routeRecognizer.add([routes[i]])
    }
  }

  // Resolve a path and return route data {handler, params}
  resolveRoute (path) {
    // Validate the path and set / to index
    path = path === '/' ? '/index' : path

    // If the route is empty route to /404 instead
    let routes = this.routeRecognizer.recognize(path)
    routes = routes || this.routeRecognizer.recognize('/404')

    // Return the route data
    return routes[0]
  }
}
