import {isClient} from './Settings'

const routes = [
  // HomePageLayout Route
  {
    path: '/index',
    handler: {
      chunkName: 'HomePageLayout',
      callback: function (params, routeHandler) {
        if (isClient) {
          require.ensure([], require => {
            let HomePageLayout = require('../Containers/HomePageLayout/HomePageLayout.jsx')
            routeHandler({
              RoutedComponent: HomePageLayout.default,
              routeParams: params
            })
          })
        }
      }
    }
  },

  // 404 RouteManager
  {
    path: '/404',
    handler: {
      chunkName: 'Error404Layout',
      callback: function (params, routeHandler) {
        if (isClient) {
          require.ensure([], require => {
            let Error404Layout = require('../Containers/Error404Layout/Error404Layout.jsx')
            routeHandler({
              RoutedComponent: Error404Layout.default,
              routeParams: params
            })
          })
        }
      }
    }
  }
]

export default routes
