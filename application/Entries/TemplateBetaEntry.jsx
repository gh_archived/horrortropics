// Import node modules
import {h, render} from 'preact'
import PreactPromiseSSR from 'preact-promise-ssr'

// Import Application Controllers
import RouteManager from '../Classes/RouteManager.js'

// Import Application Components
import Router from '../Containers/Router/Router.jsx'

// Import Application Containers
import Application from '../Containers/Application/Application.jsx'
import TemplateBetaLayout from '../Containers/TemplateBetaLayout/TemplateBetaLayout.jsx'

// Create preact promise manager and set initial data
let preactPromiseSSR = new PreactPromiseSSR()
let Provider = preactPromiseSSR.getProvider()
preactPromiseSSR.setInitialData(window._PPSSR_STORE ? window._PPSSR_STORE : {})

// Get initial routing
let routeManager = new RouteManager()
let route = routeManager.resolveRoute(window.location.pathname)

render(
  <Provider>
    <Application>
      <Router
        routeParams={route.params}
        DefaultComponent={TemplateBetaLayout}
      />
    </Application>
  </Provider>,
  document.querySelector('#mount'),
  document.querySelector('#Application')
)
