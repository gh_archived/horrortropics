// Import Express and Contentful API
import express from 'express'
import {createClient} from 'contentful'

// Setup Router
let router = express.Router()

// Route and cache the Contentful API
router.get('/api/entry/:entry', function (req, res) {
  req.apicacheGroup = 'contentful'

  let client = createClient({
    // This is the space ID. A space is like a project folder in Contentful terms
    space: 'jxx6d8sv3axe',

    // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
    accessToken:
      '19f49d9ebf0e64bc3f00ad5dedaa4fb8a6ae068f401c19fb2c903a5c19bd7cce'
  })

  // Server the json content returned from contentful
  client.getEntries({'sys.id': req.params.entry, include: 10}).then(entry => {
    res.json(entry)
  })
})

export default router
