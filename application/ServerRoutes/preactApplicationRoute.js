// Import Express and Application DOM Renderer
import express from 'express'
import Document from '../Containers/Document/Document.jsx'
const documentDom = new Document()

// Setup Router
let router = express.Router()

// Render the Preact Application
router.get('/*', function (req, res) {
  // Set cache group to default
  req.apicacheGroup = 'default'

  // Render Preact Page
  documentDom.handleRender(req, res)
})

export default router
