// Import Express and Middleware
import express from 'express'
import path from 'path'

// Setup Router
let router = express.Router()

// Send the service worker javascript
router.get('/', function (req, res, next) {
  res.sendFile(path.resolve(__dirname, '../../public/service-worker.js'))
})

export default router
