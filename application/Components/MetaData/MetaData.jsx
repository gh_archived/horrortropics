import {h, Component} from 'preact'
import Helmet from 'preact-helmet'

class MetaData extends Component {
  render () {
    return (
      <Helmet
        htmlAttributes={{
          amp: undefined,
          itemscope: true,
          lang: this.props.metadata.fields.titleAttributesLang
            ? this.props.metadata.fields.titleAttributesLang
            : 'en-GB',

          itemtype: this.props.metadata.fields.htmlSchemaUrl
            ? this.props.metadata.fields.htmlSchemaUrl
            : ''
        }}
        title={
          this.props.metadata.fields.pageTitle
            ? this.props.metadata.fields.pageTitle
            : ''
        }
        titleAttributes={{
          itemprop: 'name',
          lang: this.props.metadata.fields.titleAttributesLang
            ? this.props.metadata.fields.titleAttributesLang
            : 'en-GB'
        }}
        meta={[
          this.props.metadata.fields.metaDescription
            ? {
              name: 'description',
              content: this.props.metadata.fields.metaDescription
            }
            : {},
          this.props.metadata.fields.metaName
            ? {
              itemprop: 'name',
              content: this.props.metadata.fields.metaName
            }
            : {},
          this.props.metadata.fields.metaDescription
            ? {
              itemprop: 'description',
              content: this.props.metadata.fields.metaDescription
            }
            : {},
          this.props.metadata.fields.metaImageUrl.fields.file.url
            ? {
              itemprop: 'image',
              content: this.props.metadata.fields.metaImageUrl.fields.file.url
            }
            : {},
          this.props.metadata.fields.metaTwittercard
            ? {
              name: 'twitter:card',
              content: this.props.metadata.fields.metaTwittercard
            }
            : {},
          this.props.metadata.fields.metaTwittersite
            ? {
              name: 'twitter:site',
              content: this.props.metadata.fields.metaTwittersite
            }
            : {},
          this.props.metadata.fields.metaTwittertitle
            ? {
              name: 'twitter:title',
              content: this.props.metadata.fields.metaTwittertitle
            }
            : {},
          this.props.metadata.fields.metaTwitterdescription
            ? {
              name: 'twitter:description',
              content: this.props.metadata.fields.metaTwitterdescription
            }
            : {},
          this.props.metadata.fields.metaTwittercreator
            ? {
              name: 'twitter:creator',
              content: this.props.metadata.fields.metaTwittercreator
            }
            : {},
          this.props.metadata.fields.metaTwitterimagesrc.fields
            ? {
              name: 'twitter:image:src',
              content: this.props.metadata.fields.metaTwitterimagesrc.fields
                .file.url
            }
            : {},
          this.props.metadata.fields.metaOgtitle
            ? {
              property: 'og:title',
              content: this.props.metadata.fields.metaOgtitle
            }
            : {},
          this.props.metadata.fields.metaOgtype
            ? {
              property: 'og:type',
              content: this.props.metadata.fields.metaOgtype
            }
            : {},
          this.props.metadata.fields.metaOgurl
            ? {
              property: 'og:url',
              content: this.props.metadata.fields.metaOgurl
            }
            : {},
          this.props.metadata.fields.metaOgimage.fields.file.url
            ? {
              property: 'og:image',
              content: this.props.metadata.fields.metaOgimage.fields.file.url
            }
            : {},
          this.props.metadata.fields.metaOgdescription
            ? {
              property: 'og:description',
              content: this.props.metadata.fields.metaOgdescription
            }
            : {},
          this.props.metadata.fields.metaOgsiteName
            ? {
              property: 'og:description',
              content: this.props.metadata.fields.metaOgsiteName
            }
            : {},
          this.props.metadata.sys.createdAt
            ? {
              property: 'article:published_time',
              content: this.props.metadata.sys.createdAt
            }
            : {},
          this.props.metadata.sys.updatedAt
            ? {
              property: 'article:published_time',
              content: this.props.metadata.sys.updatedAt
            }
            : {},
          this.props.metadata.fields.metaArticlesection
            ? {
              property: 'article:section',
              content: this.props.metadata.fields.metaArticlesection
            }
            : {},
          this.props.metadata.fields.metaArticletag
            ? {
              property: 'article:tag',
              content: this.props.metadata.fields.metaArticletag
            }
            : {}
        ]}
      />
    )
  }
}

export default MetaData
