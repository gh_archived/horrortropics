// Import node modules
import {h, Component} from 'preact'

import MetaBall from '../MetaBall/MetaBall'

// Import Data
import {isClient} from '../../Data/Settings'
import metaballs from '../../Data/metaballs.json'

// Setup MetaBallEmitter Preact Component
class MetaBallEmitter extends Component {
  componentDidMount () {
    if (isClient) {
      document.querySelector('#beta').style.display = 'block'

      // setTimeout(() => {
      //   document.querySelector('#gooeymetaballs').style.transform =
      //     'translateZ(0px)'
      // }, 300)
    }
  }

  // Render Preact component
  render () {
    let forceAnimation = {
      transition: 'transform 999999s',
      transform: 'translateZ(1px)'
    }

    return (
      <g>
        <filter id='goo' filterUnits='objectBoundingBox'>
          <feTurbulence
            type='turbulence'
            baseFrequency='0.01'
            numOctaves='2'
            result='turb'
          />
          <feColorMatrix
            in='turb'
            result='huedturb'
            type='hueRotate'
            values='90'
          >
            {/* <animate
              attributeType='XML'
              attributeName='values'
              values='0;350;700'
              dur='8s'
              repeatCount='indefinite'
            /> */}
          </feColorMatrix>
          <feDisplacementMap
            in='SourceGraphic'
            in2='huedturb'
            scale='32'
            result='displace'
            xChannelSelector='B'
          />
          <feGaussianBlur result='blur1' stdDeviation='10' />
          <feColorMatrix
            in='blur1'
            mode='matrix'
            values='1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7'
            result='goo'
          />
          <feGaussianBlur in='goo' result='blur2' stdDeviation='3' />
          <feColorMatrix
            in='blur2'
            mode='matrix'
            values='1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7'
            result='goo2'
          />
        </filter>

        <defs>
          <mask id='metamask'>
            <g xstyle={forceAnimation} id='gooeymetaballs' filter='url(#goo)'>
              <MetaBall d1={metaballs[0].d1} x1={431} y1={180} />
              <MetaBall d1={metaballs[0].d2} x1={-40} y1={-40} />
              <MetaBall d1={metaballs[0].d1} x1={550} y1={441} />
            </g>
          </mask>
        </defs>
      </g>
    )
  }
}

export default MetaBallEmitter
