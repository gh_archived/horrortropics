// Import node modules
import {h, Component} from 'preact'
import PropTypes from 'prop-types'

// Import Elements
import Link from '../../Elements/link/link'
import Logo from '../../Elements/SVGs/Logo'

// Import component styles
import styles from './header.css'

class Header extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isOpen: false
    }
  }

  toggleNav () {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render () {
    return (
      <header
        className={[
          styles.header,
          this.state.isOpen ? styles['header-IsOpen'] : ''
        ].join(' ')}
      >
        <div className={styles.header__bar}>
          <div className={styles.header__logo}>
            <Link
              aria-label='Homepage'
              href='/'
              className={styles.header__logoLink}
            >
              <Logo className={styles.header__logoSvg} />
            </Link>
          </div>

          <div className={styles.header__hamburger}>
            <button
              onClick={this.toggleNav.bind(this)}
              className={styles.header__hamburgerButton}
              aria-label='Open Navigation Menu'
            >
              <span className={styles.header__hamburgerBar} />
              <span className={styles.header__hamburgerBar} />
              <span aria-hidden className={styles.header__hamburgerBar}>
                X
              </span>
            </button>
          </div>
        </div>
        <nav className={styles.header__nav}>
          <ul className={styles.header__navList}>
            <li className={styles.header__navItem}>
              <Link
                callback={this.toggleNav.bind(this)}
                className={styles.header__navLink}
                href='/#!about'
              >
                About
              </Link>
            </li>
            <li className={styles.header__navItem}>
              <Link
                callback={this.toggleNav.bind(this)}
                className={styles.header__navLink}
                href='/#!process'
              >
                Process
              </Link>
            </li>
            {/* <li className={styles.header__navItem}>
              <Link className={styles.header__navLink} href='/#!services'>
                Services
              </Link>
            </li> */}
            <li className={styles.header__navItem}>
              <Link
                callback={this.toggleNav.bind(this)}
                className={styles.header__navLink}
                href='/#!our-work'
              >
                Our Work
              </Link>
            </li>
            <li className={styles.header__navItem}>
              <Link
                callback={this.toggleNav.bind(this)}
                className={styles.header__navLink}
                href='/#!contact'
              >
                Contact
              </Link>
            </li>
          </ul>
        </nav>
        <button
          onClick={this.toggleNav.bind(this)}
          className={styles.header__closeButton}
        >
          close menu
        </button>
      </header>
    )
  }
}

Header.childContextTypes = {
  isSyncMaster: PropTypes.bool
}

export default Header
