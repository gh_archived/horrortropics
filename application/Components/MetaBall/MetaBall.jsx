// Import node modules
import {h, Component} from 'preact'

// Setup MetaBallEmitter Preact Component
class MetaBall extends Component {
  // Render Preact component
  render () {
    const height = Math.max(
      document.body.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.clientHeight,
      document.documentElement.scrollHeight,
      document.documentElement.offsetHeight
    )

    const width = Math.max(
      document.body.scrollWidth,
      document.body.offsetWidth,
      document.documentElement.clientWidth,
      document.documentElement.scrollWidth,
      document.documentElement.offsetWidth
    )

    const x = this.props.x2 ? width - this.props.x2 : this.props.x1
    const y = this.props.y2 ? height - this.props.y2 : this.props.y1

    const inline = {
      transform: `translateX(${x}px) translateY(${y}px)`
    }

    return <path style={inline} fill='#fff' d={this.props.d1} />
  }
}

export default MetaBall
