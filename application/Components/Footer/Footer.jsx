// Import node modules
import {h, Component} from 'preact'
import styles from './footer.css'

class Footer extends Component {
  render () {
    return <div className={styles.footer}>&copy; 1035000 B.C. LTD 2017</div>
  }
}

export default Footer
