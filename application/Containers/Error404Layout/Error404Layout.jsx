// Import node modules
import {h, Component} from 'preact'
import Helmet from 'preact-helmet'
import PropTypes from 'prop-types'

// Import data
import {origin} from '../../Data/Settings'

// Import Elements
import Link from '../../Elements/link/link'

// Import component styles
import styles from './error-404-layout.css'

// Setup Layout Preact Component
class Error404Layout extends Component {
  constructor (props) {
    super(props)

    this.state = {
      title: ''
    }
  }

  componentWillMount () {
    this.promiseManager = new this.context.PromiseManager()

    this.promiseManager.createPromise({
      id: 'contentful_404_entry',
      lifespan: -1,
      promise: function () {
        return fetch(`${origin}/api/entry/4DFLKch9hYKSuAgawMoIm0`).then(res =>
          res.json()
        )
      }
    })

    this.promiseManager.subscribe('contentful_404_entry', data => {
      this.setState({
        title: data.fields.title
      })
    })

    this.promiseManager.resolve()
  }

  componentWillUnmount () {
    this.promiseManager.unsubscribe('contentful_404_entry')
  }

  // Render Preact component
  render () {
    return (
      <div>
        <Helmet title={`Test Puppers // ${this.state.title}`} />
        {this.state.title}
        <Link href='/'>Test Link</Link>
      </div>
    )
  }
}

Error404Layout.contextTypes = {
  PromiseManager: PropTypes.func.isRequired
}

export default Error404Layout
