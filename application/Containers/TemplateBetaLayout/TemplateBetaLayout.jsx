// Import node modules
import { h, Component } from 'preact'

// Import component styles
import styles from './template-beta-layout.css'

// Setup Layout Preact Component
export default class TemplateBetaLayout extends Component {
  // Render Preact component
  render () {
    return (
      <div>
        {this.props.dog}<br />
        {Date.now()}<br />
        TemplateBetaLayout
      </div>
    )
  }
}
