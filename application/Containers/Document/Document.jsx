// Import node modules
import {h} from 'preact'
import PreactPromiseSSR from 'preact-promise-ssr'
import Helmet from 'preact-helmet'

// Import data
import manifest from '../../Data/manifest.json'

// Import Application Classes
import RouteManager from '../../Classes/RouteManager.js'

// Import Application Components
import Router from '../Router/Router.jsx'

// Import Application Containers
import Application from '../../Containers/Application/Application.jsx'
import Error404Layout from '../../Containers/Error404Layout/Error404Layout.jsx'
import HomePageLayout from '../../Containers/HomePageLayout/HomePageLayout.jsx'
import TemplateBetaLayout from '../../Containers/TemplateBetaLayout/TemplateBetaLayout.jsx'

// Imports webpack compile information
import stats from '../../../public/stats.json'

// Require node file system modules
const path = require('path')
const fs = require('fs')

export default class Document {
  constructor () {
    this.route = null
    this.chunkName = null
    this.RoutedLayoutComponent = null
    this.preactDOMString = ''
    this.stats = stats
    this.statsString = JSON.stringify(this.stats)
    this.routeManager = new RouteManager()
  }

  _getDefaultContainer () {
    let containers = {
      Error404Layout: Error404Layout,
      HomePageLayout: HomePageLayout,
      TemplateBetaLayout: TemplateBetaLayout
    }

    return containers[this.chunkName]
  }

  // Get vendor CSS to inject into the page (vendor === Elements/Document.css)
  _getVendorCSS () {
    // path is relative to /bin/scripts/server.js
    return fs.readFileSync(
      path.resolve(
        __dirname,
        `../../public/${this.stats.assetsByChunkName.vendor[1]}`
      ),
      'utf8'
    )
  }

  // Get critical CSS to inject into the page
  _getCriticalPathCSS () {
    // path is relative to /bin/scripts/server.js
    return fs.readFileSync(
      path.resolve(
        __dirname,
        `../../public/${this.stats.assetsByChunkName[this.chunkName][1]}`
      ),
      'utf8'
    )
  }

  // Generate HTML links to all non critical CSS files
  _getNonCriticalFiles () {
    let nonCriticalCSS = []
    let nonCriticalJS = []

    for (let chunk in this.stats.assetsByChunkName) {
      if (
        this.stats.assetsByChunkName.hasOwnProperty(chunk) &&
        chunk !== 'vendor' &&
        chunk !== 'manifest' &&
        chunk !== this.chunkName
      ) {
        let chunkObj = this.stats.assetsByChunkName[chunk]

        if (typeof chunkObj === 'object' && chunkObj.length > 1) {
          nonCriticalCSS.push({
            rel: 'stylesheet',
            type: 'text/css',
            href: this.stats.publicPath + chunkObj[1]
          })

          nonCriticalJS.push({
            type: 'text/javascript',
            src: this.stats.publicPath + chunkObj[0]
          })
        }
      }
    }

    return `
      <script>
        window._LAZY_CSS = ${JSON.stringify(nonCriticalCSS)}
        window._LAZY_JS = ${JSON.stringify(nonCriticalJS)}
      </script>
    `
  }

  handleRender (req, res) {
    // Resolve route and get routing data
    this.route = this.routeManager.resolveRoute(req.url)
    this.chunkName = this.route.handler.chunkName

    let preactPromiseSSR = new PreactPromiseSSR()
    let Provider = preactPromiseSSR.getProvider()

    preactPromiseSSR
      .renderToString(
        <Provider>
          <Application>
            <Router
              routeParams={this.route.params}
              DefaultComponent={this._getDefaultContainer()}
            />
          </Application>
        </Provider>
      )
      .then(markup => {
        let manifestPath =
          this.stats.publicPath + this.stats.assetsByChunkName.manifest

        let vendorPath =
          this.stats.publicPath + this.stats.assetsByChunkName.vendor[0]

        let entryChunkPath =
          this.stats.publicPath +
          this.stats.assetsByChunkName[this.chunkName][0]

        let preactPromiseStore = JSON.stringify(preactPromiseSSR.getStore())

        const head = Helmet.rewind()

        res.send(
          `<html lang="en-GB" prefix="og: http://ogp.me/ns#">
            <head>
              <meta charSet="utf-8" >
              <meta httpEquiv="x-ua-compatible" content="ie=edge" >
              <link rel="manifest" href="/public/manifest.json" >
              <meta name="mobile-web-app-capable" content="yes" >
              <meta name="apple-mobile-web-app-capable" content="yes" >
              <meta name="application-name" content="${manifest.name}" >
              <meta name="apple-mobile-web-app-title" content="${manifest.name}" >
              <meta name="theme-color" content="${manifest.theme_color}" >
              <meta name="msapplication-navbutton-color" content="${manifest.theme_color}" >
              <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" >
              <meta name="msapplication-starturl" content="/" >
              <meta name="viewport" content="width=device-width, initial-scale=1" >

              <link rel="icon" type="${manifest.icons[0]
    .type}" sizes="${manifest.icons[0].sizes}" href="${manifest
  .icons[0].src}" >

              <link rel="apple-touch-icon" type="${manifest.icons[0]
    .type}" sizes="${manifest.icons[0].sizes}" href="${manifest
  .icons[0].src}" >

              ${head.title.toString()}
              ${head.meta.toString()}
              <script>
                window._PPSSR_STORE = ${preactPromiseStore}
              </script>
              <script defer src="${manifestPath}"></script>
              <script defer src="${vendorPath}"></script>
              <script defer src="${entryChunkPath}"></script>
              <script defer src="/service-worker-registrar.js"></script>
              <style type="text/css">${this._getVendorCSS()}</style>
              <style type="text/css">${this._getCriticalPathCSS()}</style>
            </head>
            <body>
              <div id="mount">${markup}</div>
              <script type="text/javascript">
                window.stats = ${this.statsString};
              </script>
              ${this._getNonCriticalFiles()}
            </body>
          </html>`
        )
      })
      .catch(error => {
        console.error(error)
        res.send('503 Server Error')
      })
  }
}
