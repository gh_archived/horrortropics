// Import node modules
import {h, Component} from 'preact'
import PropTypes from 'prop-types'
import Helmet from 'preact-helmet'

// Import Application Components
import Header from '../../Components/Header/Header'
import Footer from '../../Components/Footer/Footer'

import styles from './application.css'

// Import data
import {isClient} from '../../Data/Settings'

// Import document styles also known as vendor styles
import '../../Elements/Document.css'

class Application extends Component {
  // Render Preact component
  render () {
    return (
      <div id='Application' className={styles.application}>
        <Helmet
          link={isClient ? window._LAZY_CSS : []}
          script={isClient ? window._LAZY_JS : []}
        />
        <Header />
        {this.props.children}
        <Footer />
      </div>
    )
  }
}

export default Application
