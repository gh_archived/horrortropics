// Import node modules
import {h, Component} from 'preact'

// Import Component Styles
import styles from './svg-fobject-sync.css'

class SyncContainer extends Component {
  getChildContext () {
    return {
      isSyncMaster: this.props.isSyncMaster
    }
  }

  render ({children}) {
    return (children && children[0]) || null
  }
}

// Setup Preact Component
class SVGFObjectSync extends Component {
  // Render Preact Component
  render () {
    return (
      <svg
        {...this.props}
        className={styles.svgFobjectSync}
        xmlns='http://www.w3.org/2000/svg'
        version='1.1'
      >
        <defs>
          {this.props.mask}
        </defs>

        <foreignObject x='0' y='0' width='100%' height='100%'>
          <body xmlns='http://www.w3.org/1999/xhtml'>
            <SyncContainer isSyncMaster>
              {this.props.alpha}
            </SyncContainer>
          </body>
        </foreignObject>

        <foreignObject
          className={styles.svgFobjectSync__beta}
          mask={`url(#${this.props.maskId})`}
          x='0'
          y='0'
          width='100%'
          height='100%'
        >
          <body xmlns='http://www.w3.org/1999/xhtml'>
            <SyncContainer isSyncMaster={false}>
              {this.props.beta}
            </SyncContainer>
          </body>
        </foreignObject>
      </svg>
    )
  }
}

export default SVGFObjectSync
