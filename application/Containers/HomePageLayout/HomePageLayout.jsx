// Import node modules
import {h, Component} from 'preact'
import PropTypes from 'prop-types'
import marked from 'marked'

// Import data
import {origin} from '../../Data/Settings'

// Import Application Components
import HomePageHeroSection from './Sections/HomePageHeroSection'
import MetaData from '../../Components/MetaData/MetaData'

// Import component styles
import styles from './home-page-layout.css'

// Setup Preact Component
class HomePageLayout extends Component {
  constructor (props) {
    super(props)

    this.state = {
      metadata: false,
      article: false,
      ourWorkSection: false
    }
  }

  componentWillMount () {
    this.promiseManager = new this.context.PromiseManager()

    this.promiseManager.createPromise({
      id: 'contentful_entry_page_index',
      lifespan: -1,
      promise: function () {
        return fetch(`${origin}/api/entry/7om5PvxhVSgwOIUQwAIqqU`).then(res =>
          res.json()
        )
      }
    })

    this.promiseManager.subscribe('contentful_entry_page_index', data => {
      this.setState({
        metadata: Object.assign({}, data.items[0].fields.metadata),
        article: data.items[0].fields.article,
        ourWorkSection: [...data.items[0].fields.ourWorkSection]
      })
    })

    this.promiseManager.resolve()
  }

  componentWillUnmount () {
    this.promiseManager.unsubscribe('contentful_entry_page_index')
  }

  // Render Preact component
  render () {
    this.ourWorkHtml = []
    if (typeof this.state.ourWorkSection === 'object') {
      for (let i = 0; i < this.state.ourWorkSection.length; i++) {
        let iValue = this.state.ourWorkSection[i]

        if (iValue.fields.content) {
          this.ourWorkHtml.push(
            <section
              dangerouslySetInnerHTML={{
                __html: marked(iValue.fields.content)
              }}
            />
          )
        }

        for (let ii = 0; ii < iValue.fields.pictures.length; ii++) {
          let iiValue = iValue.fields.pictures[ii]

          this.ourWorkHtml.push(
            <picture>
              <source
                media='(min-width: 1440px)'
                srcset={`${iiValue.fields.desktopHd.fields.file.url}, ${iiValue
                  .fields.desktopHd2x.fields.file.url} 2x`}
              />

              <source
                media='(min-width: 1024px)'
                srcset={`${iiValue.fields.desktop.fields.file.url}, ${iiValue
                  .fields.desktop2x.fields.file.url} 2x`}
              />
              <source
                media='(min-width: 768px)'
                srcset={`${iiValue.fields.tablet.fields.file.url}, ${iiValue
                  .fields.tablet2x.fields.file.url} 2x`}
              />

              <img
                src={iiValue.fields.mobile.fields.file.url}
                srcset={`${iiValue.fields.mobile2x.fields.file.url} 2x`}
                alt={iiValue.fields.altText}
              />
            </picture>
          )
        }
      }
    }

    return (
      <div className={styles.homePageLayout}>
        <HomePageHeroSection />
        <article>
          <section
            dangerouslySetInnerHTML={{
              __html: this.state.article ? marked(this.state.article) : ''
            }}
          />

          {this.ourWorkHtml}
          <section>
            <h1 id='contact'>Contact</h1>
            <p>
              If you have a project you want to talk about or simply want to get
              in touch email us at{' '}
              <a href='mailto:graham@horrorandtropics.com'>
                graham@horrorandtropics.com
              </a>{' '}
              or call us at <a href='tel:+16044459097'>+1 (604) 445 - 9097</a>
            </p>
          </section>
        </article>
        {this.state.metadata ? <MetaData metadata={this.state.metadata} /> : ''}
      </div>
    )
  }
}

export default HomePageLayout
