// Import node modules
import {h, Component} from 'preact'

// Import component styles
import styles from './home-page-hero-section.css'
import HorrorTropics from '../../../Elements/SVGs/HorrorTropics'

// Setup Layout Preact Component
class HomePageHeroSection extends Component {
  // Render Preact component
  render () {
    return (
      <div className={styles.homePageHeroSection}>
        <HorrorTropics
          aira-label='Horror and Tropics an interactive digital media agency'
          className={styles.homePageHeroSection__title}
        />
      </div>
    )
  }
}

export default HomePageHeroSection
