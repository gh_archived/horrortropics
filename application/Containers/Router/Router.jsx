// Import node modules
import {h, Component} from 'preact'

// Import Application Classes
import RouteManager from '../../Classes/RouteManager'

// Import data
import {isClient} from '../../Data/Settings'

// Import styles
import style from './router.css'

export default class Router extends Component {
  constructor (props) {
    super(props)

    this.subscriptions = {}

    // Set default states
    // Convert params passed from Entry points to state params
    this.state = {
      RoutedComponent: this.props.DefaultComponent,
      routeParams: this.props.routeParams ? this.props.routeParams : {}
    }
  }

  getChildContext () {
    return {
      routeSub: this.routeSub.bind(this),
      routeUnSub: this.routeUnSub.bind(this)
    }
  }

  routeSub (key, callback) {
    this.subscriptions[key] = callback
  }

  routeUnSub (key) {
    if (this.subscriptions.hasOwnProperty(key)) delete this.subscriptions[key]
  }

  routerPub () {
    for (var i = 0; i < this.subscriptions.length; i++) {
      this.subscriptions[i]()
    }
  }

  // Setup navigation event callbacks
  watchLocation () {
    if (isClient) {
      document.addEventListener(
        'historyRouteUpdate',
        this.updateRoute.bind(this),
        false
      )

      window.onpopstate = this.updateRoute.bind(this)
    }
  }

  // Update route displayed to the user
  updateRoute () {
    console.log('route updated', window.location.pathname)
    let route = this.routeManager.resolveRoute(window.location.pathname)
    route.handler.callback(route.params, states => {
      this.setState(states)
    })

    this.routerPub()
  }

  // Run browser setup functions when component mounts
  componentDidMount () {
    if (isClient) {
      this.routeManager = new RouteManager()
      this.watchLocation()
      this.updateRoute()
    }
  }

  // Render Preact component
  render () {
    return (
      <div className={style.router}>
        <this.state.RoutedComponent {...this.state.routeParams} />
      </div>
    )
  }
}
