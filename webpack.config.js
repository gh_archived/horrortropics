const webpack = require('webpack')

// builtin node path module
const path = require('path')

// It moves every require('style.css') in entry chunks into a separate css output file.
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const CleanWebpackPlugin = require('clean-webpack-plugin')

var CopyWebpackPlugin = require('copy-webpack-plugin')

// Setup Entry Points
const browserEntry = {
  Error404Layout: path.join(
    __dirname,
    './application/Entries/Error404Entry.jsx'
  ),

  HomePageLayout: path.join(
    __dirname,
    './application/Entries/HomePageEntry.jsx'
  ),

  TemplateBetaLayout: path.join(
    __dirname,
    './application/Entries/TemplateBetaEntry.jsx'
  ),

  vendor: ['preact']
}

const serverEntry = {
  server: path.join(__dirname, './application/www.js')
}

module.exports = function (env) {
  const nodeEnv = env.build // process.env.NODE_ENV || 'development'
  const isProd = env.build === 'production'
  const isClient = env.base === 'client'
  const isWatching = env.hasOwnProperty('watch') && env.watch === 'watching'

  console.log({
    isProd: isProd,
    isClient: isClient
  })

  return {
    devtool: isProd ? false : 'eval-source-map',

    node: isClient
      ? {}
      : {
        console: !isProd,
        global: false,
        process: false,
        Buffer: false,
        __filename: false,
        __dirname: false
      },

    entry: isClient ? browserEntry : serverEntry,

    output: {
      path: isClient
        ? path.join(__dirname, './public')
        : path.join(__dirname, './bin'),
      filename:
        isClient && isProd
          ? 'scripts/[chunkhash].[name].bundle.js'
          : 'scripts/[name].js',
      publicPath: '/public/'
    },

    resolve: {
      extensions: ['.js', '.jsx', '.json', '.css']
    },

    target: isClient ? 'web' : 'node',

    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          options: {
            plugins: ['syntax-async-functions'],
            presets: isClient
              ? [['env', {targets: {browsers: ['last 2 versions']}}]]
              : []
          }
        },
        {
          test: /\.json$/,
          use: 'json-loader'
        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  importLoaders: 1,
                  camelCase: 'dashesOnly',
                  modules: true,
                  localIdentName: '[hash:base64:5]___[local]',
                  minimize: true
                }
              },
              'postcss-loader'
            ]
          })
        }
      ]
    },

    plugins: isClient
      ? [
        isProd
          ? new CleanWebpackPlugin([
            './public/scripts',
            './public/styles',
            './public/service-worker.js',
            './public/service-worker-registrar.js',
            './public/manifest.json'
          ])
          : new CleanWebpackPlugin([]),

        new ExtractTextPlugin({
          filename:
              isClient && isProd
                ? 'styles/[contenthash].[name].css'
                : 'styles/[name].css'
        }),

        new webpack.DefinePlugin({
          ONSERVER: false,
          'process.env': {NODE_ENV: JSON.stringify(nodeEnv)}
        }),

        new webpack.optimize.CommonsChunkPlugin({
          name: ['vendor', 'manifest']
        }),

        new CopyWebpackPlugin([
          {
            from: 'application/Data/manifest.json',
            to: 'manifest.json'
          },
          {
            from: 'application/service-worker.js',
            to: 'service-worker.js'
          },
          {
            from: 'application/service-worker-registrar.js',
            to: 'service-worker-registrar.js'
          }
        ]),

        function () {
          this.plugin('done', function (stats) {
            require('fs').writeFileSync(
              path.join(__dirname, 'public', 'stats.json'),
              JSON.stringify({
                publicPath: stats.toJson().publicPath,
                assetsByChunkName: stats.toJson().assetsByChunkName
              })
            )
          })
        },

        function () {
          this.plugin('done', function (stats) {
            if (!isWatching) {
              require('fs').writeFileSync(
                path.join(__dirname, 'public', 'stats-full.json'),
                JSON.stringify(stats.toJson())
              )
            }
          })
        }
      ]
      : [
        new CleanWebpackPlugin(['./bin/scripts']),

        new webpack.optimize.CommonsChunkPlugin({
          name: [],
          minChunks: Infinity
        }),

        new ExtractTextPlugin({
          filename: 'styles/[contenthash].[name].css'
        }),

        new webpack.DefinePlugin({
          ONSERVER: true,
          'process.env': {NODE_ENV: JSON.stringify(nodeEnv)}
        })
      ]
  }
}
